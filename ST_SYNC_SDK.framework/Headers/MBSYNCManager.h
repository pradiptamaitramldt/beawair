//
//  Created by alvin on 20-10-13.
//  Copyright (c) 2020 meeblue. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>
#import <CoreLocation/CoreLocation.h>


typedef NS_ENUM(NSInteger, MB_SYNC_ERROR_CODE) {
    MB_SYNC_ERROR_CODE_CONNECT_FAILED,
    MB_SYNC_ERROR_CODE_LINK_LOSS,
    MB_SYNC_ERROR_CODE_CONNECTING_TIMEOUT,
    MB_SYNC_ERROR_CODE_AUTHENTICATION_FAILED,
    MB_SYNC_ERROR_CODE_TRANSMISSION_ERROR,
};

typedef struct
{
    uint16_t m_Beacon_Broadcast;
    uint16_t m_Trigger_Mode_Adv_Time;//5-65534
    uint8_t m_Beacon_State;
    int8_t m_txPower;
    uint8_t m_Connect_State;//Unit Minute, the max allow to keep connected, 0 for no limit.
    uint8_t m_Motion_Strength_One;
    uint8_t m_Motion_Strength_Two;
    uint8_t m_alarm_type;
    int8_t Limit_Rssi;/*Range From -40--- -100*/
    uint8_t Scan_Interval;//Unit Second，range from 1-30, default 1;
    uint32_t m_current_time_stamp;
    uint32_t record_latest_time_stamp;//latest record time stamp
    uint16_t battery_votage;
    uint8_t    mDevice_Name[20];
    uint16_t    m_white_list_count;
    uint8_t    Systemp_ID[6];
    uint8_t    Firmware_ID[20];
} Device_Data_Struct_t;

@class MBSYNCManager;


@protocol MBSYNCManagerDelegate <NSObject>
@optional

/*!
 *  @method MBSYNCManager:didFailToSyncSTDevice:error:
 *
 *  @param manager      The  manager providing this information.
 *  @param Device_ID   The  device id of ST Device failed to sync.
 *  @param error_code        The cause of the failure.
 *
 *  @discussion         This method will report the device when trying to sync history data , but failed.
 *
 */
- (void) MBSYNCManager:(MBSYNCManager *)manager didFailToSyncSTDevice:(uint32_t)Device_ID error_code:(MB_SYNC_ERROR_CODE)error_code;


/*!
 *  @method - (void) MBSYNCManager:didDiscoverSTDevice:battery_voltage:latest_record_time_stamp:record_count:rssi
 *
 *  @param manager              The manager providing this update.
 *  @param Device_ID   The  device id of ST Device.
 *  @param batt  the battery voltage of this device
 *  @param latest_record_time_stamp  Timestamp of the latest record.
 *  @param record_count  Total number of alarm data currently stored.
 *  @param rssi                 The current rssi of <i>peripheral</i>, in dBm. A value of <code>127</code> is reserved and indicates the RSSI
 *								was not available.
 */
- (void) MBSYNCManager:(MBSYNCManager *)manager didDiscoverSTDevice:(uint32_t)Device_ID battery_voltage:(uint16_t)batt latest_record_time_stamp:(uint32_t)latest_record_time_stamp record_count:(uint16_t)record_count rssi:(int)rssi;

/*!
 *  @method MBSYNCManager:didConnectPeripheral:
 *
 *  @param manager              The manager providing this update.
 *  @param Device_ID   The  device id of ST Device.
 *  @param BasicInfo  Basic information of this device
 *  @param record_data The record data of current device .
 *  @param record_count  Total number of alarm data currently stored.
 *  @param rssi                 The current rssi of <i>peripheral</i>, in dBm. A value of <code>127</code> is reserved and indicates the RSSI
 *                                was not available.
 *
 */
- (void) MBSYNCManager:(MBSYNCManager *)manager didSyncHistoryDataWithDevice:(uint32_t)Device_ID BasicInfo:(Device_Data_Struct_t)BasicInfo rssi:(int)rssi record_count:(uint16_t)record_count record_data:(NSArray*)record_data;


/*!
*  @method MBSYNCManager:didConnectPeripheral:
*
*  @param manager              The manager providing this update.
*  @param Device_ID   The  device id of ST Device.
*  @param State  Whether the cleanup was successfully
*  @discussion         This method is invoked when a connection initiated by {@link notify_record_data_upload_to_server_state_with_device_id:} has succeeded or failed.
*
*/
- (void) MBSYNCManager:(MBSYNCManager *)manager didCleanedDeviceRecordDataWithState:(BOOL)State Device_ID:(uint32_t)Device_ID;

@required

/*!
 * @method MBSYNCManager:didFailToUSEBLE:
 * @discussion Faild to use BLE.
 */
- (void) MBSYNCManager:(MBSYNCManager *)manager didFailToUSEBLE:(NSInteger)state Message:(NSString*)Message;

@end


@interface MBSYNCManager : NSObject

@property (nonatomic,strong) id <MBSYNCManagerDelegate> delegate;

/*!
 *  @method initWithDelegate:AutoSync:AuthCode
 *
 *  @param delegate id <MBSYNCManagerDelegate>
 *  @param AutoSync Whether to enable automatic synchronization.
 *  @param AuthCode The default authentication information, the default value is "meeble", can not be null
 *
 *  @return self, a new object of this method
 *
 *  @discussion			init the manager, you must init first then you can use this function
 *
 */
- (id) initWithDelegate: (id <MBSYNCManagerDelegate>)delegate AutoSync:(BOOL)AutoSync AuthCode:(NSString *)AuthCode;


/*!
 * @method startSyncAilableDeviceAndSync
 * @discussion Start scanning for available devices
 */

- (void) startSyncAilableDeviceAndSync:(BOOL)state AutoRepeat:(BOOL)AutoRepeat;

/*!
 *  @method stopScan:
 *
 *  @discussion            stop scan the device
 *
 */
- (void)    stopScan;

/*!
 * @method setSyncAilableDeviceState
 * @param state Whether to automatically synchronize data
 */

- (void) setSyncAilableDeviceAutoState:(BOOL)state;


/*!
 * @method clearSyncSuccessList
 * @discussion This method will clean up the synchronized device id, and then these devices can be uploaded repeatedly
 */

- (void) clearSyncSuccessList;

/*!
* @method clear_record_data_with_device_id:
* @param  state
 The local data should be cleaned up after uploading the data to the server, The status must be reported to the system even if it is no need to cleaned up
*/
- (void) notify_record_data_upload_to_server_state_with_device_id:(uint32_t)device_id state:(BOOL)state clean_local_data:(BOOL)clean;
@end
