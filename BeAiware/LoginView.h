//
//  LoginView.h
//  BeAiware
//
//  Created by Pradipta Maitra on 31/07/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AFNetworking.h"
#import "Reachability.h"
#import "BeaconsController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginView : UIViewController<NSURLSessionDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

- (IBAction)buttonSignInAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
