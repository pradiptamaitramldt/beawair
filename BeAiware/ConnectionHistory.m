//
//  ConnectionHistory.m
//  BeAiware
//
//  Created by Pradipta Maitra on 30/07/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import "ConnectionHistory.h"

@implementation ConnectionHistory

-(instancetype)initWithData:(NSString *)data andTime:(NSString *)time {
    
    self = [super init];
    if (self) {
        self.data = data;
        self.time = time;
    }
    return self;
}

@end
