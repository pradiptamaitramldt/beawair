//
//  Created by alvin on 20-10-12.
//  Copyright (c) 2020 meeblue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ST_SYNC_SDK/MBSTSDK.h>
#import "AppDelegate.h"

@interface AlarmHistoryViewController : UIViewController<MBBLEManagerDelegate, MBBLEDeviceDelegate, UITextFieldDelegate>
{
    AppDelegate *appDelegate;
}

@property (strong, nonatomic) MBBLEManager *manager;
@property (strong, nonatomic) MBBLEDevice *mBeacon;

@end
