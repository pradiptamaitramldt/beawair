//
//  Created by alvin on 19-10-22.
//  Copyright (c) 2019 meeblue. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController

+ (BOOL) GET_AUTO_SEND_AUTHENtICATIOIN_STATE;
+ (void) SET_AUTO_SEND_AUTHENtICATIOIN_STATE:(BOOL)Value;
+ (NSString*) GET_AUTO_SEND_AUTHENtICATIOIN_CODE;
+ (void) SET_AUTO_SEND_AUTHENtICATIOIN_CODE:(NSString*)Value;
@end
