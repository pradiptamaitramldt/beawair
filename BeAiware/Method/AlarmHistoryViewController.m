//
//  Created by alvin on 20-10-12.
//  Copyright (c) 2020 meeblue. All rights reserved.
//

#import "AlarmHistoryViewController.h"
#import "WaitProgressShow.h"
#import "Utils.h"

@interface AlarmHistoryViewController ()
{
    Record_Configure_Data_t Record_Configure;
    NSMutableArray *m_History;
    
    Reading_Sensor_State_t Reading_State;
    
    NSThread *mExpordThread;
    
    int m_curent_percent;
}

typedef struct
{
    uint32_t time;
    uint32_t data;
} Alert_data_t;

@property (weak, nonatomic) IBOutlet UITableView *m_table_view;
@property (weak, nonatomic) IBOutlet UIButton *m_Btn_Send;

@end

@implementation AlarmHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

-(void) StartExpord:(NSString*)temp
{
    NSString *DocPath = [Utils CreatRecordExportTempFile:[NSData dataWithBytes:self.mBeacon.m_beacon_all_data.Systemp_ID length:6]];
    
    NSString *str = [NSString stringWithFormat:@"ID%@Save time%@Device ID %@Total\n", CSV_FENGE_CODE,CSV_FENGE_CODE,CSV_FENGE_CODE];
    NSMutableString *csvString = [NSMutableString string];
    [csvString appendString:str];
    
    NSArray *TEMPArray = [NSArray arrayWithArray:m_History];
    int percent = 0;
    
    for (int i = 0; i< TEMPArray.count; i ++) {
        Alert_data_t Value;
        uint64_t TEMP = [[TEMPArray objectAtIndex:i] unsignedLongLongValue];
        
        memcpy(&Value, &TEMP, sizeof(Alert_data_t));
        float floattemp = TEMPArray.count;
        floattemp = i/floattemp*100;
        int per = floattemp;
        if (per > percent) {
            percent = per;
            NSString *SHOW = [NSString stringWithFormat:@"Trying to generate output files. Finished:%d%%",per];
            [self performSelectorOnMainThread:@selector(ShowState:) withObject:SHOW waitUntilDone:true];
        }
        
        long ID = (Value.data & 0xFFFFFFFF);
        
        str = [NSString stringWithFormat:@"%d%@%@%@%ld%@%lu\n", i+1, CSV_FENGE_CODE, [self dateToStringTime:[NSDate dateWithTimeIntervalSince1970:Value.time]], CSV_FENGE_CODE,  ID, CSV_FENGE_CODE, (unsigned long)TEMPArray.count];
        
        [csvString appendString:str];
    };
    
    NSData *data = [csvString dataUsingEncoding:NSUTF8StringEncoding];
    [data writeToFile:DocPath atomically:YES];
    
    [self performSelectorOnMainThread:@selector(Share:) withObject:DocPath waitUntilDone:YES];
    [mExpordThread cancel];
}

-(void) ShowState:(NSString*)str
{
    [WaitProgressShow showWithStatus:str];
}

-(void) Share:(NSString*)str
{
    [WaitProgressShow dismiss];
    [Utils shareData:[NSURL fileURLWithPath:str] View:self];
}

- (void) export_data:(NSTimer *)timer
{
    if (m_History.count > 20000) {
        [self performSelectorInBackground:@selector(StartExpord:) withObject:nil];
    }
    else{
        [self StartExpord:nil];
    }
}

- (IBAction)OnTouchFinish:(id)sender {
    if (m_History.count > 0) {
        [WaitProgressShow showWithStatus:@"Trying to generate output files"];
        [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(export_data:) userInfo:nil repeats:false];
    }
    else{
        [WaitProgressShow showErrorWithStatus:@"No data available to export"];
    }
    
}

- (void) ReadConfigureData
{
    m_curent_percent = 0;
    [WaitProgressShow showWithStatus:@"Trying to sync data..."];
    [self.mBeacon readValueForService:MEEBLUE_CLOSE_SERVICE Characteristic:MEEBLUE_CLOSE_STATE completion:^(BOOL State, NSData *value, NSError *error) {
        
        [value getBytes:&self->Reading_State length:sizeof(Record_Configure_Data_t)];
        if (State) {
            [self.mBeacon notifyValueForService:MEEBLUE_CLOSE_SERVICE Characteristic:MEEBLUE_CLOSE_DATA State:1 completion:^(BOOL value, NSError *error)
            {
                if (value) {
                        Record_Configure_Data_t temp;
                        temp.current_time = [[NSDate date] timeIntervalSince1970];
                        temp.sync_max_count = 0;//0 for all data
                        temp.option_command = 0x81;//sync time and data
                        temp.option_size = 22;//this must be 22
                        self->Reading_State.start_time = temp.current_time;
                        [self.mBeacon writeValueForService:[NSData dataWithBytes:&temp length:sizeof(Record_Configure_Data_t)] Service:MEEBLUE_CLOSE_SERVICE Characteristic:MEEBLUE_CLOSE_STATE completion:^(BOOL value, NSError *error) {
                            if (value) {
                            }
                            else{
                                [WaitProgressShow showErrorWithStatus:@"Sync failed"];
                            }
                        }];
                }
                else{
                    [WaitProgressShow showErrorWithStatus:@"Sync failed"];
                }
                
            } Notify:^(BOOL State, NSData *value, NSError *error) {
                NSLog(@"Data:%@", value);
                if (!(value.length%8 == 0 && value.length >= 16))
                {
                    [WaitProgressShow showErrorWithStatus:@"Current Beacon Not Supported"];
                    return ;
                }
                int length = (int)value.length/8;
                    
                uint64_t readvalue[length];
                [value getBytes:readvalue length:length*sizeof(uint64_t)];
                for (int i = 0; i < length; i++) {
                    if (readvalue[i] != 0xFFFFFFFFFFFFFFFF) {
                        [self->m_History addObject:[NSNumber numberWithUnsignedLongLong:readvalue[i]]];
                        self->Reading_State.current_count += 1;
                    }
                    else{
                        self->Reading_State.end_time = [[NSDate date] timeIntervalSince1970];
                        [self.m_table_view reloadData];
                        [WaitProgressShow dismiss];
                        [self Save_New_Data];
                        return;
                    }
                }
                
                float total = self->Record_Configure.current_max_count;
                float current = self->Reading_State.current_count;
                total = current/total*100;
                int per = total;
                if (per > self->m_curent_percent) {
                    self->m_curent_percent = per;
                    NSString *STRR = [NSString stringWithFormat:@"Has synchronized %d records", self->Reading_State.current_count];
                    [WaitProgressShow showWithStatus:STRR];
                }
                
            }];
        }
        else{
            [WaitProgressShow showErrorWithStatus:@"Sync failed"];
        }
    }];
}

-(void) Save_New_Data
{
    NSArray *his = [NSArray arrayWithArray:m_History];
        
    NSArray *sortedArray = [his sortedArrayUsingComparator: ^(NSNumber* obj1, NSNumber* obj2) {
        
        Alert_data_t Value1;
        Alert_data_t Value2;
        uint64_t TEMP1 = [obj1 unsignedLongLongValue];
        uint64_t TEMP2 = [obj2 unsignedLongLongValue];
        
        memcpy(&Value1, &TEMP1, sizeof(Alert_data_t));
        memcpy(&Value2, &TEMP2, sizeof(Alert_data_t));
        if (Value1.time > Value2.time) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        else{
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    m_History = [NSMutableArray arrayWithArray:sortedArray];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init];
    backItem.title=@"";
    self.navigationItem.backBarButtonItem=backItem;
    
    [self setTitle:@"Alert Record"];
    
    m_History = [[NSMutableArray alloc] init];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    
    self.mBeacon.delegate = self;
    
    Record_Configure = self.mBeacon.m_beacon_all_data.Record_Config;
    [_m_Btn_Send setTitle:@"Export" forState:UIControlStateNormal];
    [self ReadConfigureData];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    self.mBeacon.delegate = self;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}


#pragma mark - JLBLEManagerDelegate Callbacks


-(void) MBBLEManager:(MBBLEManager *)central didDisconnectPeripheral:(MBBLEDevice *)peripheral error:(NSError *)error
{
    [WaitProgressShow showErrorWithStatus:@"Disconnect from peripheral"];
    [self.navigationController popToRootViewControllerAnimated:true];
    self.mBeacon.delegate = Nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 5;
            break;
        case 1:
            return [NSArray arrayWithArray:m_History].count;
            break;
    }
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if ([NSArray arrayWithArray:m_History].count == 0) {
        return 1;
    }
    return 2;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Sync State";
            break;
        case 1:
            return @"Alert History Data";
            break;
    }

    return @"";
}


- (NSString *)dateToStringTime:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForConnectable:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"ITEM_Message"];
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    cell.accessoryType = UITableViewCellAccessoryNone;
    switch (indexPath.row) {
        case 0:
            [Title setText:@"Latest Record Time"];
        {
            Alert_data_t Value;
            uint64_t TEMP = [[m_History firstObject] unsignedLongLongValue];
            if (TEMP == 0) {
                [Message setText:@"--"];
            }
            else{
                memcpy(&Value, &TEMP, sizeof(Alert_data_t));
                [Message setText:[self dateToStringTime:[NSDate dateWithTimeIntervalSince1970:Value.time]]];
            }
            
        }
            
            break;
        case 1:
            [Title setText:@"Oldest Record Time"];
            {
                Alert_data_t Value;
                uint64_t TEMP = [[m_History lastObject] unsignedLongLongValue];
                if (TEMP == 0) {
                    [Message setText:@"--"];
                }
                else{
                    memcpy(&Value, &TEMP, sizeof(Alert_data_t));
                    [Message setText:[self dateToStringTime:[NSDate dateWithTimeIntervalSince1970:Value.time]]];
                }
            }
            break;
        case 2:
            [Title setText:@"Time Consuming for Sync"];
        {
            uint32_t seconed = Reading_State.end_time - Reading_State.start_time;
            [Message setText:[NSString stringWithFormat:@"%d second(s)", seconed]];
        }
            break;
        case 3:
            [Title setText:@"Totel Count Sync from Device"];
            [Message setText:[NSString stringWithFormat:@"%d", Reading_State.current_count]];
            break;
        case 4:
            [Title setText:@"Clear All Flash Data"];
            [Message setText:@"Clear"];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        default:
            break;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForTrigger:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"ITEM_Message"];
    cell.accessoryType = UITableViewCellAccessoryNone;
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    
    Alert_data_t Value;
    
    uint64_t TEMP = [[[NSArray arrayWithArray:m_History] objectAtIndex:indexPath.row] unsignedLongLongValue];
    memcpy(&Value, &TEMP, sizeof(Alert_data_t));
    [Title setText:[self dateToStringTime:[NSDate dateWithTimeIntervalSince1970:Value.time]]];
    long ID = (Value.data & 0xFFFFFFFF);
    [Message setText:[NSString stringWithFormat:@"ID:%ld", ID]];
     
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
            cell = [self tableView:tv cellForConnectable:indexPath];
            break;
        case 1:
            cell = [self tableView:tv cellForTrigger:indexPath];
            break;
        default:
            break;
    }
    return cell;
}

- (void) Clear_All_Data
{
    Record_Configure_Data_t temp;
    temp.current_time = [[NSDate date] timeIntervalSince1970];
    temp.sync_max_count = 0;
    temp.current_max_count = 0;
    temp.option_command = 0xC0;//clear all data and sync the time
    temp.option_size = 0;
    self->Reading_State.start_time = temp.current_time;
    self->Reading_State.end_time = temp.current_time;
    [WaitProgressShow showWithStatus:@"Trying to clear flash data"];
    [self.mBeacon writeValueForService:[NSData dataWithBytes:&temp length:sizeof(Record_Configure_Data_t)] Service:MEEBLUE_CLOSE_SERVICE Characteristic:MEEBLUE_CLOSE_STATE completion:^(BOOL value, NSError *error) {
        if (value) {
            self->Reading_State.current_count = 0;
            [self->m_History removeAllObjects];
            [self.m_table_view reloadData];
            [WaitProgressShow dismiss];
        }
        else{
            [WaitProgressShow showErrorWithStatus:@"Clear failed"];
        }
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 4://清理所有数据
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Do you really sure to clear all flash data?" message:nil preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                        
                    }];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Sure" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        [self Clear_All_Data];
                    }];
                    
                    [alertController addAction:cancelAction];
                    [alertController addAction:okAction];
                    
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                    
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0://Triggger By INT1
                default:
                    break;
            }
            break;
        default:
            break;
    }
}

@end
