//
//  Created by alvin on 20-10-12.
//  Copyright (c) 2020 meeblue. All rights reserved.
//

#import "AlarmStateViewController.h"
#import "WaitProgressShow.h"

typedef void(^ALVINStringCompletionBlock) (BOOL state, NSString* value);

@interface AlarmStateViewController ()
{
    ALERT_State_Data_t Beacon_State_Data;
}


@property (weak, nonatomic) IBOutlet UITableView *m_table_view;

@end

@implementation AlarmStateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (IBAction)OnTouchFinish:(id)sender {
    
}

- (void) Save_data:(NSIndexPath*)indexPath
{
    [WaitProgressShow showWithStatus:@"Trying to configure..."];
    
    [self.mBeacon writeValueForService:[NSData dataWithBytes:&Beacon_State_Data length:sizeof(ALERT_State_Data_t)] Service:MEEBLUE_MAIN_SERVICE Characteristic:MEEBLUE_MAIN_BEACON_STATE completion:^(BOOL value, NSError *error) {
        if (value) {
            Beacon_All_Data_t ALL = self.mBeacon.m_beacon_all_data;
            ALL.Alert_State = self->Beacon_State_Data;
            [self.mBeacon update_All_Data:ALL];
            [WaitProgressShow dismiss];
            [self.m_table_view reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else{
            [WaitProgressShow showErrorWithStatus:@"Configure failed"];
        }
    }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init];
    backItem.title=@"";
    self.navigationItem.backBarButtonItem=backItem;
    
    [self setTitle:@"Adv Type"];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view, typically from a nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    
    self.mBeacon.delegate = self;
    
    Beacon_State_Data = self.mBeacon.m_beacon_all_data.Alert_State;
}

-(void) JLBLEDevice:(MBBLEDevice *)device peripheralDidUpdateRSSI:(NSError *)error
{
    
}

-(void) viewDidAppear:(BOOL)animated
{
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    self.mBeacon.delegate = self;
    
    [self.m_table_view reloadData];
}

#pragma mark - JLBLEManagerDelegate Callbacks


-(void) JLBLEManager:(MBBLEManager *)central didDisconnectPeripheral:(MBBLEDevice *)peripheral error:(NSError *)error
{
    [WaitProgressShow showErrorWithStatus:@"Disconnect from peripheral"];
    [self.navigationController popToRootViewControllerAnimated:true];
    self.mBeacon.delegate = Nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 3;
            break;
    }
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Select Alarm Type When Closed To Each Other";
            break;
    }

    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForAdvertiseMode:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"ITEM_Check"];
    UILabel *Title = [cell viewWithTag:1];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    switch (indexPath.row) {
        case 0:
            [Title setText:@"LED"];
            if (Beacon_State_Data.m_alarm_type & 0x01) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            break;
        case 1:
            [Title setText:@"Buzzer"];
            if (Beacon_State_Data.m_alarm_type & 0x02) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            break;
        case 2:
            [Title setText:@"Vibrate"];
            if (Beacon_State_Data.m_alarm_type & 0x04) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            break;
        default:
            break;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
            cell = [self tableView:tv cellForAdvertiseMode:indexPath];
            break;
        default:
            break;
    }
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    Beacon_State_Data.m_alarm_type ^= 0x01;
                    [self Save_data:indexPath];
                    break;
                case 1:
                    Beacon_State_Data.m_alarm_type ^= 0x02;
                    [self Save_data:indexPath];
                    break;
                case 2:
                    Beacon_State_Data.m_alarm_type ^= 0x04;
                    [self Save_data:indexPath];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
}

@end
