//
//  Created by alvin on 20-10-12.
//  Copyright (c) 2020 meeblue. All rights reserved.
//

#import "MeeblueAlarmStateController.h"
#import "WaitProgressShow.h"
#import "AlarmHistoryViewController.h"
#import "AlarmStateViewController.h"
#import "WhiteListViewController.h"
#import "Utils.h"

typedef void(^ALVINStringCompletionBlock) (BOOL state, NSString* value);

@interface MeeblueAlarmStateController ()
{
    
}
@property (weak, nonatomic) IBOutlet UITableView *m_table_view;

@property (weak, nonatomic) IBOutlet UIButton *m_Refresh_Btn;

@end

@implementation MeeblueAlarmStateController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (IBAction)OnTouchRefresh:(id)sender {
    [self ReadDataFromHere:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init];
    backItem.title=@"";
    self.navigationItem.backBarButtonItem=backItem;
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view, typically from a nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    
    
    self.mBeacon.delegate = self;
    [self ReadDataFromHere:nil];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    self.mBeacon.delegate = self;
    
    [self.m_table_view reloadData];
}

-(void) ReadDataFromHere:(NSString*)temp
{
    //开始读取特征值
    [WaitProgressShow showWithStatus:@"Trying to read Beacon's Configuration"];
    [self.mBeacon readAllDataFromBeacon];
}

- (void)MBBLEDevice:(MBBLEDevice *)peripheral ValueReadFinished:(BOOL)Value error:(NSError *)error
{
    [WaitProgressShow dismiss];
    [self.m_table_view reloadData];
}

-(void) waitViewDissmiss
{
    [WaitProgressShow dismiss];
}

#pragma mark - JLBLEManagerDelegate Callbacks


-(void) MBBLEManager:(MBBLEManager *)central didDisconnectPeripheral:(MBBLEDevice *)peripheral error:(NSError *)error
{
    [WaitProgressShow showErrorWithStatus:@"Disconnect from peripheral"];
    [self.navigationController popToRootViewControllerAnimated:true];
    self.mBeacon.delegate = Nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0://beacon state
            return 7;
            break;
        case 1://储存的数据量
            return 1;
            break;
        case 2:
            return 4;
            break;
        case 3://Device information
            return 5;
        case 4://导出所有数据
            return 1;
            break;
    }
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 5;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Beacon State Control";
            break;
        case 1:
            return @"Close Alert Data";
            break;
        case 2:
            return @"Device Funtion";
            break;
        case 3:
            return @"Device Information";
            break;
        case 6:
            return @"Configurations For production";
            break;
        default:
            break;
    }
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForState:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"Information"];
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    
    switch (indexPath.row) {
        case 0:
        {
            cell = [tv dequeueReusableCellWithIdentifier:@"Information"];
            Title = [cell viewWithTag:1];
            Message = [cell viewWithTag:2];
            [Title setText:@"Device ID:"];
            long Device_ID = self.mBeacon.m_beacon_all_data.Alert_State.m_device_id & 0xFFFFFFFF;
            [Message setText:[NSString stringWithFormat:@"%ld", Device_ID]];            
        }
            break;
        case 1:
            [Title setText:@"Keep Connect Max Time:"];
            if (self.mBeacon.m_beacon_all_data.Alert_State.m_Connect_State == 0) {
                [Message setText:[NSString stringWithFormat:@"No limit"]];
            }
            else{
                [Message setText:[NSString stringWithFormat:@"%d min", self.mBeacon.m_beacon_all_data.Alert_State.m_Connect_State]];
            }
            break;
        case 2:
            [Title setText:@"Signal Limit >=:"];
            [Message setText:[NSString stringWithFormat:@"%d", self.mBeacon.m_beacon_all_data.Alert_State.Limit_Rssi]];
            break;
        case 3:
            [Title setText:@"Alert Delay Time:"];
            [Message setText:[NSString stringWithFormat:@"%d s", self.mBeacon.m_beacon_all_data.Alert_State.Scan_Interval]];
            break;
        case 4:
            [Title setText:@"Tx Power Code:"];
            [Message setText:[NSString stringWithFormat:@"%d dBm", self.mBeacon.m_beacon_all_data.Alert_State.m_txPower]];
            break;
        case 5:
        {
            [Title setText:@"Alarm Type:"];
            NSString *ShowString = @"";
            if (self.mBeacon.m_beacon_all_data.Alert_State.m_alarm_type == 0 || (self.mBeacon.m_beacon_all_data.Alert_State.m_alarm_type & 0x07) == 00) {
                ShowString = @"None";
            }
            else{
                if (self.mBeacon.m_beacon_all_data.Alert_State.m_alarm_type & 0x01) {
                    ShowString = [ShowString stringByAppendingString:@"+LED"];
                }
                if (self.mBeacon.m_beacon_all_data.Alert_State.m_alarm_type & 0x02) {
                    ShowString = [ShowString stringByAppendingString:@"+Buzzer"];
                }
                if (self.mBeacon.m_beacon_all_data.Alert_State.m_alarm_type & 0x04) {
                    ShowString = [ShowString stringByAppendingString:@"+Vibrate"];
                }
                ShowString = [ShowString substringFromIndex:1];
            }
            [Message setText:ShowString];
        }
            break;
        case 6:
            [Title setText:@"White List Count:"];
            [Message setText:[NSString stringWithFormat:@"%d", [self.mBeacon get_white_list_count]]];
            break;
        default:
            break;
    }
    return cell;
}

- (NSString *)dateToStringTime:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForINformation:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"Information_no_cation"];
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    
    switch (indexPath.row) {
        case 0://name
            cell = [tv dequeueReusableCellWithIdentifier:@"Information"];
            Title = [cell viewWithTag:1];
            Message = [cell viewWithTag:2];
            [Title setText:@"Device Name:"];
            [Message setText:[NSString stringWithFormat:@"%@", [NSString stringWithUTF8String:(char*)self.mBeacon.m_beacon_all_data.mDevice_Name]]];
            break;
        case 1://batt
            [Title setText:@"Battery Voltage:"];
            [Message setText:[NSString stringWithFormat:@"%d mv", self.mBeacon.m_beacon_all_data.m_batt_voltage]];
            break;
        case 2://Device Time
            [Title setText:@"Time From Device:"];
            [Message setText:[NSString stringWithFormat:@"%@", [self dateToStringTime:[NSDate dateWithTimeIntervalSince1970:self.mBeacon.m_beacon_all_data.Alert_State.m_current_time_stamp]]]];
            break;
        case 3://firmware ID
            [Title setText:@"Firmware Version:"];
            [Message setText:[NSString stringWithUTF8String:(const char*)self.mBeacon.m_beacon_all_data.Firmware_ID]];
            break;
        case 4://Device ID
            [Title setText:@"Device Identifier:"];
            [Message setText:[NSString stringWithFormat:@"%@", [Utils convertDataToHexStr:[NSData dataWithBytes:self.mBeacon.m_beacon_all_data.Systemp_ID length:6]].uppercaseString]];
            break;
        default:
            break;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForAlert:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"Information_no_cation"];
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    
    switch (indexPath.row) {
        case 0://count
            cell = [tv dequeueReusableCellWithIdentifier:@"Information"];
            Title = [cell viewWithTag:1];
            Message = [cell viewWithTag:2];
            [Title setText:@"Sync All Alert Data:"];
            [Message setText:[NSString stringWithFormat:@"%d", self.mBeacon.m_beacon_all_data.Record_Config.current_max_count]];
            break;
        default:
            break;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForPeripheral:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"Information"];
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    
    switch (indexPath.row) {
        case 0://1st motion
            [Title setText:@"Find The Beacon"];
            [Message setText:@""];
            break;
        case 2://1st motion
            [Title setText:@"INT1 Motion Detect:"];
            [Message setText:@"Disabled"];
            if ([self.mBeacon findCharacteristicFromUUID:MEEBLUE_ACC_SERVICE CharacteristicUUID:MEEBLUE_MOTION_1ST].isNotifying) {
                [Message setText:@"Enabled"];
            }
            
            break;
        case 3://temp
            [Title setText:@"INT2 Motion Detect:"];
            [Message setText:@"Disabled"];
            if ([self.mBeacon findCharacteristicFromUUID:MEEBLUE_ACC_SERVICE CharacteristicUUID:MEEBLUE_MOTION_2ND].isNotifying) {
                [Message setText:@"Enabled"];
            }
            break;
        case 1://humi
            [Title setText:@"Button Detect:"];
            [Message setText:@"Disabled"];
            if ([self.mBeacon findCharacteristicFromUUID:MEEBLUE_PERIPHERAL_SERVICE CharacteristicUUID:MEEBLUE_PERIPHERAL_BUTTON].isNotifying) {
                [Message setText:@"Enabled"];
            }
            break;
        default:
            break;
    }
    
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tv cellForProduct:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"Information"];
    UILabel *Title = [cell viewWithTag:1];
    UILabel *Message = [cell viewWithTag:2];
    
    switch (indexPath.row) {
        case 0:
            [Title setText:@"Export All Configurations For Production"];
            [Message setText:@""];
            break;
        default:
            break;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
            cell = [self tableView:tv cellForState:indexPath];
            break;
        case 1:
            cell = [self tableView:tv cellForAlert:indexPath];
            break;
        case 2:
            cell = [self tableView:tv cellForPeripheral:indexPath];
            break;
        case 3:
            cell = [self tableView:tv cellForINformation:indexPath];
            break;
        case 4:
            cell = [self tableView:tv cellForProduct:indexPath];
            break;
        default:
            break;
    }
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"AlertSensor"])
    {
        AlarmHistoryViewController *detailViewController = (AlarmHistoryViewController*) segue.destinationViewController;
        detailViewController.mBeacon = self.mBeacon;
    }
    else if ([segue.identifier isEqualToString: @"WhiteList"])
    {
        WhiteListViewController *detailViewController = (WhiteListViewController*) segue.destinationViewController;
        detailViewController.mBeacon = self.mBeacon;
    }
    else if ([segue.identifier isEqualToString: @"AlarmType"])
    {
        AlarmStateViewController *detailViewController = (AlarmStateViewController*) segue.destinationViewController;
        detailViewController.mBeacon = self.mBeacon;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                {
                    if (self.mBeacon.m_beacon_all_data.Alert_State.m_device_id != 0) {
                        [WaitProgressShow showErrorWithStatus:@"Device ID Can Not Be Changed"];
                        return;
                    }
                    [self ConfigureData:@"Range from 1 to 4294967295" keyboardType:UIKeyboardTypeNumberPad completion:^(BOOL state, NSString* value){
                        uint32_t InputData = [value intValue];
                        if (state && InputData >= 1 && InputData <= 4294967295) {
                            Beacon_All_Data_t  temp = self.mBeacon.m_beacon_all_data;
                            temp.Alert_State.m_device_id = InputData;
                            [self Data_ReloadData:temp Name:false];
                        }
                        else{
                            [WaitProgressShow showErrorWithStatus:@"Data entered is not acceptable"];
                        }
                    }];
                }
                    break;
                case 1://2nd Advertise Channel
                {
                    [self ConfigureData:@"Range from 0 to 255" keyboardType:UIKeyboardTypeNumberPad completion:^(BOOL state, NSString* value){
                        uint8_t InputData = [value intValue];
                        if (state && InputData >= 0 && InputData <= 255) {
                            Beacon_All_Data_t  temp = self.mBeacon.m_beacon_all_data;
                            temp.Alert_State.m_Connect_State = InputData;
                            [self Data_ReloadData:temp Name:false];
                        }
                        else{
                            [WaitProgressShow showErrorWithStatus:@"Data entered is not acceptable"];
                        }
                    }];
                }
                    break;
                case 2:
                {
                    [self ConfigureData:@"Range from -100 to -40" keyboardType:UIKeyboardTypeNumbersAndPunctuation completion:^(BOOL state, NSString* value){
                        int8_t InputData = [value intValue];
                        if (state && InputData >= -100 && InputData <= -40)
                        {
                            Beacon_All_Data_t  temp = self.mBeacon.m_beacon_all_data;
                            temp.Alert_State.Limit_Rssi = InputData;
                            [self Data_ReloadData:temp Name:false];
                        }
                        else{
                            [WaitProgressShow showErrorWithStatus:@"Data entered is not acceptable"];
                        }
                    }];
                }
                    break;
                case 3:
                {
                    [self ConfigureData:@"Range from 1 to 30" keyboardType:UIKeyboardTypeNumberPad completion:^(BOOL state, NSString* value){
                        uint8_t InputData = [value intValue];
                        if (state && InputData >= 1 && InputData <= 30)
                        {
                            Beacon_All_Data_t  temp = self.mBeacon.m_beacon_all_data;
                            temp.Alert_State.Scan_Interval = InputData;
                            [self Data_ReloadData:temp Name:false];
                        }
                        else{
                            [WaitProgressShow showErrorWithStatus:@"Data entered is not acceptable"];
                        }
                    }];
                }
                    break;
                case 4://Tx Power Code
                {
                    [self ConfigureData:@"Input aviliable int value" keyboardType:UIKeyboardTypeNumbersAndPunctuation completion:^(BOOL state, NSString* value){
                        int8_t InputData = [value intValue];
                        {
                            Beacon_All_Data_t  temp = self.mBeacon.m_beacon_all_data;
                            temp.Alert_State.m_txPower = InputData;
                            [self Data_ReloadData:temp Name:false];
                        }
                    }];
                }
                    break;
                case 5:
                    [self performSegueWithIdentifier:@"AlarmType" sender:nil];
                    break;
                case 6:
                    [self performSegueWithIdentifier:@"WhiteList" sender:nil];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch (indexPath.row) {
                case 0://Beacon State
                    [self performSegueWithIdentifier:@"AlertSensor" sender:nil];
                    break;
                default:
                    break;
            }
            break;
        case 2:
            switch (indexPath.row) {
                case 0://Find The Beacon
                {
                    uint8_t value = 0x01;
                    [WaitProgressShow showWithStatus:@"Sending command..."];
                    [self.mBeacon writeValueForService:[NSData dataWithBytes:&value length:1] Service:MEEBLUE_PERIPHERAL_SERVICE Characteristic:MEEBLUE_PERIPHERAL_BUZZER completion:^(BOOL value, NSError* error){
                        if (value) {
                            [WaitProgressShow dismiss];
                        }
                        else{
                            [WaitProgressShow showErrorWithStatus:@"Command send failed"];
                        }
                    }];
                }
                    break;
                case 2://1st Channel Motion Detect
                {
                    [WaitProgressShow showWithStatus:@"Setting..."];
                    [self.mBeacon notifyValueForService:MEEBLUE_ACC_SERVICE Characteristic:MEEBLUE_MOTION_1ST State:2 completion:^(BOOL value, NSError* error){

                        if (value) {
                            [self.m_table_view reloadData];
                            [WaitProgressShow dismiss];
                        }
                        else{
                            [WaitProgressShow showErrorWithStatus:@"Failed to notify INT1"];
                        }
                    } Notify:^(BOOL State, NSData *value, NSError *error) {
                        [WaitProgressShow showSuccessWithStatus:@"INT1 trigger detected" duration:0.2];
                    }];
                }
                    break;
                    
                case 3://2st Channel Motion Detect
                    {
                        [WaitProgressShow showWithStatus:@"Setting..."];
                        [self.mBeacon notifyValueForService:MEEBLUE_ACC_SERVICE Characteristic:MEEBLUE_MOTION_2ND State:2 completion:^(BOOL value, NSError* error){

                            if (value) {
                                [self.m_table_view reloadData];
                                [WaitProgressShow dismiss];
                            }
                            else{
                                [WaitProgressShow showErrorWithStatus:@"Failed to notify INT2"];
                            }
                        } Notify:^(BOOL State, NSData *value, NSError *error) {
                            [WaitProgressShow showSuccessWithStatus:@"INT2 trigger detected" duration:0.2];
                        }];
                    }
                    break;
                case 1://Button Detect
                    {
                        [WaitProgressShow showWithStatus:@"Setting..."];
                        [self.mBeacon notifyValueForService:MEEBLUE_PERIPHERAL_SERVICE Characteristic:MEEBLUE_PERIPHERAL_BUTTON State:2 completion:^(BOOL value, NSError* error){

                            if (value) {
                                [self.m_table_view reloadData];
                                [WaitProgressShow dismiss];
                            }
                            else{
                                [WaitProgressShow showErrorWithStatus:@"Failed to notify button"];
                            }
                        } Notify:^(BOOL State, NSData *value, NSError *error) {
                            char TEMP[value.length];
                            [value getBytes:TEMP length:value.length];
                            
                            if (TEMP[0] == 0x02) {
                                [WaitProgressShow showSuccessWithStatus:@"Button long press trigger detected" duration:0.5];
                            }
                            else if (TEMP[0] == 0x01){
                                [WaitProgressShow showSuccessWithStatus:@"Button press trigger detected" duration:0.5];
                            }
                        }];
                    }
                    break;
                default:
                    break;
            }
            break;
        case 3:
             switch (indexPath.row) {
                 case 0://Name
                     {
                         [self ConfigureData:@"length from 1 to 15" keyboardType:UIKeyboardTypeASCIICapable completion:^(BOOL state, NSString* value){
                             uint16_t InputData = value.length;
                             if (state && InputData >= 1 && InputData <= 15) {
                                 Beacon_All_Data_t  temp = self.mBeacon.m_beacon_all_data;
                                 memset(temp.mDevice_Name, 0x00, 20);
                                 memcpy(temp.mDevice_Name, value.UTF8String, value.length);
                                 [self Data_ReloadData:temp Name:true];
                             }
                             else{
                                 [WaitProgressShow showErrorWithStatus:@"Data entered is not acceptable"];
                             }
                         }];
                     }
                     break;
                 default:
                     break;
             }
            break;
        case 4:
            if (indexPath.row == 0) {
                [self ExportAllConfigure];
            }
            break;
        default:
            break;
    }
}

-(void) Data_ReloadData:(Beacon_All_Data_t)temp Name:(BOOL)Name
{
    [self.mBeacon update_All_Data:temp];
    [WaitProgressShow showWithStatus:@"Trying to configure..."];
    
    if (Name) {
        int lenth = 0;
        uint8_t mDevice_Name[20];
        memset(mDevice_Name, 0, 20);
        for (lenth = 0; lenth < 20; lenth++) {
            if (self.mBeacon.m_beacon_all_data.mDevice_Name[lenth] != 0) {
                mDevice_Name[lenth] = self.mBeacon.m_beacon_all_data.mDevice_Name[lenth];
            }
            else{
                break;
            }
        }
        
        [self.mBeacon writeValueForService:[NSData dataWithBytes:mDevice_Name length:lenth] Service:MEEBLUE_MAIN_SERVICE Characteristic:MEEBLUE_MAIN_DEVICE_NAME completion:^(BOOL value, NSError *error) {
            if (value) {
                [self.m_table_view reloadData];
                [WaitProgressShow dismiss];
            }
            else{
                [WaitProgressShow showErrorWithStatus:@"Configure failed"];
            }
            
        }];
    }
    else
    {
        ALERT_State_Data_t Beacon_state = self.mBeacon.m_beacon_all_data.Alert_State;
        
        [self.mBeacon writeValueForService:[NSData dataWithBytes:&Beacon_state length:sizeof(ALERT_State_Data_t)] Service:MEEBLUE_MAIN_SERVICE Characteristic:MEEBLUE_MAIN_BEACON_STATE completion:^(BOOL value, NSError *error) {
            if (value) {
                [WaitProgressShow dismiss];
                [self.m_table_view reloadData];
            }
            else{
                [WaitProgressShow showErrorWithStatus:@"Configure failed"];
            }
        }];
    }
}

-(void) ConfigureData:(NSString*)placeholder keyboardType:(UIKeyboardType)keyboardType completion:(ALVINStringCompletionBlock)completion;
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Input Data" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Finish" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *textField = alertController.textFields[0];
        [alertController dismissViewControllerAnimated:YES completion:nil];
        if (textField.text.length>0) {
            completion(true, textField.text);
        }
        else{
            completion(false, nil);
        }
        
        
    }];
    UIAlertAction *Continue = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
//        completion(false, nil);
    }];
        
    [alertController addAction:okAction];
    [alertController addAction:Continue];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = placeholder;
        textField.keyboardType = keyboardType;
    }];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void) ExportAllConfigure
{
    [WaitProgressShow showWithStatus:@"Trying to generate configuration files for production"];
    [self Local_StartExpord:nil];
}

-(void) Local_StartExpord:(NSString*)temp
{
    NSString *MAC = [NSString stringWithFormat:@"%@", [Utils convertDataToHexStr:[NSData dataWithBytes:self.mBeacon.m_beacon_all_data.Systemp_ID length:6]].uppercaseString];
    NSString *fileNameStr = [NSString stringWithFormat:@"%@_Configuration_%@.csv", MAC, [Utils SaveToStringTime:[NSDate date]]];
    NSString *DocPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:fileNameStr];

    NSFileManager * fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:DocPath]){
        [ fileManager removeItemAtPath:DocPath error:NULL];
    }
    
    Beacon_All_Data_t ALL_DATA = self.mBeacon.m_beacon_all_data;
    
    NSString *str = [NSString stringWithFormat:@"ID%@Service UUID%@Characteristic UUID%@Hex/String Value%@Length\n", CSV_FENGE_CODE,CSV_FENGE_CODE,CSV_FENGE_CODE,CSV_FENGE_CODE];
    NSMutableString *csvString = [NSMutableString string];
    [csvString appendString:str];

    NSString* TEMPSTR = [NSString stringWithUTF8String:(const char*)ALL_DATA.Firmware_ID];
    
    //firmware version
    str = [NSString stringWithFormat:@"%d%@%@%@%@%@%@%@%lu\n", 1, CSV_FENGE_CODE, Device_INFO_SERVER.UUIDString, CSV_FENGE_CODE, Device_INFO_FIRMWARE_ID.UUIDString, CSV_FENGE_CODE, TEMPSTR, CSV_FENGE_CODE, (unsigned long)TEMPSTR.length];
    [csvString appendString:str];
    
    //Name
    TEMPSTR = [NSString stringWithUTF8String:(const char*)ALL_DATA.mDevice_Name];
    str = [NSString stringWithFormat:@"%d%@%@%@%@%@%@%@%lu\n", 2, CSV_FENGE_CODE, MEEBLUE_MAIN_SERVICE.UUIDString, CSV_FENGE_CODE, MEEBLUE_MAIN_DEVICE_NAME.UUIDString, CSV_FENGE_CODE, TEMPSTR, CSV_FENGE_CODE, (unsigned long)TEMPSTR.length];
    [csvString appendString:str];
    
    //State
    TEMPSTR = [Utils convertDataToHexStr:[NSData dataWithBytes:&ALL_DATA.Alert_State length:sizeof(ALERT_State_Data_t)]].uppercaseString;
    str = [NSString stringWithFormat:@"%d%@%@%@%@%@0x%@%@%lu\n", 3, CSV_FENGE_CODE, MEEBLUE_MAIN_SERVICE.UUIDString, CSV_FENGE_CODE, MEEBLUE_MAIN_BEACON_STATE.UUIDString, CSV_FENGE_CODE, TEMPSTR, CSV_FENGE_CODE, sizeof(ALERT_State_Data_t)];
    [csvString appendString:str];

    NSData *data = [csvString dataUsingEncoding:NSUTF8StringEncoding];
    [data writeToFile:DocPath atomically:YES];
    [self Share:DocPath];
}

-(void) Share:(NSString*)str
{
    [WaitProgressShow dismiss];
    [Utils shareData:[NSURL fileURLWithPath:str] View:self];
}

@end
