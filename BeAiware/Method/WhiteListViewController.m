//
//  Created by alvin on 20-10-12.
//  Copyright (c) 2020 meeblue. All rights reserved.
//

#import "WhiteListViewController.h"
#import "WaitProgressShow.h"

typedef void(^ALVINStringCompletionBlock) (BOOL state, NSString* value);

@interface WhiteListViewController ()
{    
    uint32_t    m_white_list[WIHITE_LIST_LENGTH];
}


@property (weak, nonatomic) IBOutlet UITableView *m_table_view;

@end

@implementation WhiteListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    
}

- (IBAction)OnTouchFinish:(id)sender {
    
    memset(m_white_list, 0x00, WIHITE_LIST_TOTAL_LENGTH);
    [WaitProgressShow showWithStatus:@"Trying to configure..."];

    [self.mBeacon writeValueForService:[NSData dataWithBytes:m_white_list length:WIHITE_LIST_TOTAL_LENGTH] Service:MEEBLUE_CLOSE_SERVICE Characteristic:MEEBLUE_CLOSE_WHITE_LIST completion:^(BOOL value, NSError *error) {
        if (value) {
            Beacon_All_Data_t ALL = self.mBeacon.m_beacon_all_data;
            memcpy(ALL.m_white_list, self->m_white_list, WIHITE_LIST_TOTAL_LENGTH);
            [self.mBeacon update_All_Data:ALL];
            [WaitProgressShow dismiss];
            [self.m_table_view reloadData];
        }
        else{
            [WaitProgressShow showErrorWithStatus:@"Configure failed"];
        }
    }];
}

- (void) Save_data:(NSIndexPath*)indexPath
{
    [WaitProgressShow showWithStatus:@"Trying to configure..."];
    
    [self.mBeacon writeValueForService:[NSData dataWithBytes:m_white_list length:WIHITE_LIST_TOTAL_LENGTH] Service:MEEBLUE_CLOSE_SERVICE Characteristic:MEEBLUE_CLOSE_WHITE_LIST completion:^(BOOL value, NSError *error) {
        if (value) {
            Beacon_All_Data_t ALL = self.mBeacon.m_beacon_all_data;
            memcpy(ALL.m_white_list, self->m_white_list, WIHITE_LIST_TOTAL_LENGTH);
            [self.mBeacon update_All_Data:ALL];
            [WaitProgressShow dismiss];
            [self.m_table_view reloadData];
        }
        else{
            [WaitProgressShow showErrorWithStatus:@"Configure failed"];
        }
    }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init];
    backItem.title=@"";
    self.navigationItem.backBarButtonItem=backItem;
    
    [self setTitle:@"Adv Type"];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view, typically from a nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    
    self.mBeacon.delegate = self;
    
    memcpy(m_white_list, self.mBeacon.m_beacon_all_data.m_white_list, WIHITE_LIST_TOTAL_LENGTH);
}


-(void) viewDidAppear:(BOOL)animated
{
    self.manager = appDelegate.mBLEManager;
    self.manager.delegate = self;
    self.mBeacon.delegate = self;
    
    [self.m_table_view reloadData];
}

-(void) MBBLEDevice:(MBBLEDevice *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}


#pragma mark - MBBLEManagerDelegate Callbacks


-(void) MBBLEManager:(MBBLEManager *)central didDisconnectPeripheral:(MBBLEDevice *)peripheral error:(NSError *)error
{
    [WaitProgressShow showErrorWithStatus:@"Disconnect from peripheral"];
    [self.navigationController popToRootViewControllerAnimated:true];
    self.mBeacon.delegate = Nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            if ([self.mBeacon get_white_list_count] < WIHITE_LIST_LENGTH) {
                return [self.mBeacon get_white_list_count]+1;
            }
            return [self.mBeacon get_white_list_count];
            break;
    }
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"The Device ID Contains In List Will Not Alarm But Will Be Record";
            break;
        case 1:
            return @"Select Advertise Data In Standby Mode";
            break;
    }

    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForWhiteList:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:@"ITEM_Check"];
    UILabel *Title = [cell viewWithTag:1];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.row == [self.mBeacon get_white_list_count]) {
        [Title setText:@"Add new device ID"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else{
        [Title setText:[NSString stringWithFormat:@"Device ID: %d", m_white_list[indexPath.row]]];
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.section) {
        case 0:
            cell = [self tableView:tv cellForWhiteList:indexPath];
            break;
        default:
            break;
    }
    return cell;
}


-(BOOL) add_white_list_count:(uint32_t) Device_ID
{
    uint8_t i = 0;
    for (i = 0; i < WIHITE_LIST_LENGTH; i++)
    {
        if (m_white_list[i] == 0)
        {
            m_white_list[i] = Device_ID;
            return true;
        }
    }
    return false;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == [self.mBeacon get_white_list_count]) {
        [self ConfigureData:@"Range from 1 to 4294967295" keyboardType:UIKeyboardTypeNumberPad completion:^(BOOL state, NSString* value){
            uint32_t InputData = [value intValue];
            if (state && InputData >= 1 && InputData <= 4294967295) {
                
                BOOL state = [self add_white_list_count:InputData];
                if (state) {
                    [self Save_data:indexPath];
                }
                else{
                    [WaitProgressShow showErrorWithStatus:@"Add new device ID failed"];
                }
            }
            else{
                [WaitProgressShow showErrorWithStatus:@"Data entered is not acceptable"];
            }
        }];
    }
}

-(void) ConfigureData:(NSString*)placeholder keyboardType:(UIKeyboardType)keyboardType completion:(ALVINStringCompletionBlock)completion;
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Input Data" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Finish" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *textField = alertController.textFields[0];
        [alertController dismissViewControllerAnimated:YES completion:nil];
        if (textField.text.length>0) {
            completion(true, textField.text);
        }
        else{
            completion(false, nil);
        }
        
        
    }];
    UIAlertAction *Continue = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
//        completion(false, nil);
    }];
        
    [alertController addAction:okAction];
    [alertController addAction:Continue];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = placeholder;
        textField.keyboardType = keyboardType;
    }];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
