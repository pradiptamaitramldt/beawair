//
//  Created by alvin on 19-10-22.
//  Copyright (c) 2019 meeblue. All rights reserved.
//

#import "SettingViewController.h"
#import "WaitProgressShow.h"

typedef void(^StringCompletionBlock) (BOOL state, NSString* value);

@interface SettingViewController ()<UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
}

@property (weak, nonatomic) IBOutlet UITableView *m_table_view;
@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"Settings"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)OnTouchSwitch:(id)sender {
    UISwitch *TEMP = sender;
    [SettingViewController SET_AUTO_SEND_AUTHENtICATIOIN_STATE:TEMP.isOn];
    [self.m_table_view reloadData];
}

- (IBAction)OnTouchSave:(id)sender {

}


-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Tip: Use a default Authentication code";
}


- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell = [tv dequeueReusableCellWithIdentifier:@"GlobalSwitch"];
            
            UILabel *Title = (UILabel*) [cell.contentView viewWithTag:1];
            
            [Title setText:@"Auto send the code:"];
            
            UISwitch *Switch = (UISwitch*) [cell.contentView viewWithTag:2];
            
            [Switch setOn:[SettingViewController GET_AUTO_SEND_AUTHENtICATIOIN_STATE] animated:false];
            [Switch addTarget:self action:@selector(OnTouchSwitch:) forControlEvents:UIControlEventValueChanged];
        }
        else
        {
            cell = [tv dequeueReusableCellWithIdentifier:@"GlobalInput"];
            UILabel *Title = (UILabel*) [cell.contentView viewWithTag:1];
            
            [Title setText:@"Authentication code:"];
            
            UILabel *Input = [cell.contentView viewWithTag:2];
            [Input setText:[SettingViewController GET_AUTO_SEND_AUTHENtICATIOIN_CODE]];
        }
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView  deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                [SettingViewController SET_AUTO_SEND_AUTHENtICATIOIN_STATE:![SettingViewController GET_AUTO_SEND_AUTHENtICATIOIN_STATE]];
                [tableView reloadData];
                break;
            case 1:
            {
                [self ConfigureData:@"Six digit letters or numbers" keyboardType:UIKeyboardTypeASCIICapable completion:^(BOOL state, NSString* value){
                    if (!state) {
                        return;
                    }
                    if (value.length != 6) {
                        [WaitProgressShow showErrorWithStatus:@"Six digits letters or numbers required"];
                        return;
                    }
                    
                    [SettingViewController SET_AUTO_SEND_AUTHENtICATIOIN_CODE:value];
                    [WaitProgressShow showSuccessWithStatus:@"Saved Successfully"];
                    [tableView reloadData];
                }];
            }

                break;
            default:
                break;
        }
    }

}

-(void) ConfigureData:(NSString*)placeholder keyboardType:(UIKeyboardType)keyboardType completion:(StringCompletionBlock)completion;
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Input Data" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Finish" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *textField = alertController.textFields[0];
        [alertController dismissViewControllerAnimated:YES completion:nil];
        if (textField.text.length>0) {
            completion(true, textField.text);
        }
        else{
            completion(false, nil);
        }
        
        
    }];
    UIAlertAction *Continue = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
//        completion(false, nil);
    }];
        
    [alertController addAction:okAction];
    [alertController addAction:Continue];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = placeholder;
        textField.keyboardType = keyboardType;
    }];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

+ (BOOL) GET_AUTO_SEND_AUTHENtICATIOIN_STATE
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"AUTO_SEND_AUTHENtICATIOIN_STATE"] boolValue];
}

+ (void) SET_AUTO_SEND_AUTHENtICATIOIN_STATE:(BOOL)Value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:Value forKey:@"AUTO_SEND_AUTHENtICATIOIN_STATE"];
    [defaults synchronize];
}

+ (NSString*) GET_AUTO_SEND_AUTHENtICATIOIN_CODE
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"AUTO_SEND_AUTHENtICATIOIN_CODE"];
}

+ (void) SET_AUTO_SEND_AUTHENtICATIOIN_CODE:(NSString*)Value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Value forKey:@"AUTO_SEND_AUTHENtICATIOIN_CODE"];
    [defaults synchronize];
}

@end
