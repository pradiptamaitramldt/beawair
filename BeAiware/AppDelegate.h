//
//  AppDelegate.h
//  BeAiware
//
//  Created by Pradipta Maitra on 28/07/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WaitProgressShow.h"

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

