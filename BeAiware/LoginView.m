//
//  LoginView.m
//  BeAiware
//
//  Created by Pradipta Maitra on 31/07/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import "LoginView.h"

@interface LoginView ()

@end

@implementation LoginView
@synthesize textFieldEmail, textFieldPassword;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.textFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.textFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

-(void)login {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable) {
        //connection unavailable
        [self showNoInternetMessage];
        [self.loader stopAnimating];
    } else {
        NSString *uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setValue:uniqueIdentifier forKey:@"deviceToken"];
        [params setValue:@"IOS" forKey:@"deviceType"];
        [params setValue:self.textFieldEmail.text forKey:@"email"];
        [params setValue:self.textFieldPassword.text forKey:@"password"];
        
        NSError *error;
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:@"http://3.132.72.42:1931/api/login"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"json...%@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.loader stopAnimating];
                NSString *responseCode = [NSString stringWithFormat:@"%@",json[@"response_code"]];
                NSString *msg = json[@"response_message"];
                if ([responseCode isEqualToString:@"2000"]) {
                    
                    NSString *authToken = [NSString stringWithFormat:@"%@",json[@"response_data"][@"authToken"]];
                    NSString *userID = [NSString stringWithFormat:@"%@",json[@"response_data"][@"_id"]];
                    NSString *email = [NSString stringWithFormat:@"%@",json[@"response_data"][@"email"]];
                    NSLog(@"auth...%@", authToken);
                    NSLog(@"id...%@", userID);
                    NSLog(@"email...%@", email);
                    
                    NSString * storyboardName = @"Main";
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                    BeaconsController * vc = [storyboard instantiateViewControllerWithIdentifier:@"BeaconsController"];
                    [self.navigationController pushViewController:vc animated:true];
                }else{
                    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Alert" message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                                {
                                                    /** What we write here???????? **/
                                                    NSLog(@"you pressed Yes, please button");
                                                    
                                                    // call method whatever u need
                                                }];
                    
                    [alert addAction:yesButton];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }
            });
        }];
        
        [postDataTask resume];
    }
}

-(void)showNoInternetMessage{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"No Internet!"
                                  message:@"Please check your internet connection!"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)errorAlert:(NSString*)errorMessage {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:errorMessage
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(BOOL)isValidEmail: (NSString *)email {
       NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
       NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];

       return [emailTest evaluateWithObject:email];
}

- (IBAction)buttonSignInAction:(id)sender {
    
    if ([self.textFieldEmail.text  isEqualToString: @""]) {
        [self errorAlert:@"Please enter email"];
    }else if (![self isValidEmail:textFieldEmail.text]){
        [self errorAlert:@"Please enter valid email"];
    }else if ([self.textFieldPassword.text isEqualToString:@""]) {
        [self errorAlert:@"Please enter password"];
    }else{
        [self.loader startAnimating];
        [self login];
    }
}
@end
