//
//  Created by alvin on 19-10-22.
//  Copyright (c) 2019 meeblue. All rights reserved.
//

#import "BeaconsController.h"

@interface BeaconsController () <UITabBarDelegate ,UITabBarControllerDelegate, UITableViewDelegate, UITableViewDataSource>
{
}

@property (weak, nonatomic) IBOutlet UITextView *m_text_show;
@property (strong, nonatomic) MBSYNCManager *mSyncManager;

@property (strong, nonatomic) NSMutableArray *mUploadList;

@end


@interface ST_DEVICE : NSObject

@property (nonatomic)   uint32_t     device_id;
@property (nonatomic)   int     upload_count;
@property (nonatomic,strong)   NSString     *state;
@property (nonatomic) BOOL uploadFinished;

@end

@implementation ST_DEVICE
-(BOOL)isEqual:(id)object{
    ST_DEVICE *temp = object;
    if (self.device_id == temp.device_id) {
        return true;
    }
    return false;
}
@end

@implementation BeaconsController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.tabBarController.delegate = self;
    //UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init];
    //backItem.title=@"Back";
    //self.navigationItem.backBarButtonItem=backItem;
    //self.navigationController.navigationBar.translucent = NO;
    //self.mDataTableView.tableFooterView=[[UIView alloc]init];
    
    _mSyncManager = [[MBSYNCManager alloc] initWithDelegate:self AutoSync:true AuthCode:@"meeble"];
    
    deviceFOundcont = 0;
    yellowColor = [UIColor colorWithRed:255.0/255 green:204.0/255 blue:0 alpha:1.0];
    orangeColor = [UIColor colorWithRed:255.0/255 green:149.0/255 blue:0 alpha:1.0];
    greenColor = [UIColor colorWithRed:76.0/255 green:217.0/255 blue:100.0/255 alpha:1.0];
    
    self.view.backgroundColor = yellowColor;
    infoArray = [[NSMutableArray alloc] init];
    
    _mUploadList = [[NSMutableArray alloc] init];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.labelBeaconStatus.text = @"Scanning Beacons...";
    
    [self.m_text_show setText:@"Scanning Beacons..."];
    
    
    self.table_view.delegate = self;
    self.table_view.dataSource = self;
    
    [self SetCount:0];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_mSyncManager stopScan];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void) SetCount:(int)Count
{
    beaconCount = Count;
    NSLog(@"beaconCounts......>>>>>> %d",Count);
    //[self.mBeaconCountView setTitle:[NSString stringWithFormat:@"Refresh:%d", Count] forState:UIControlStateNormal];
    //[self.mDataTableView reloadData];
}

-(void) viewDidAppear:(BOOL)animated
{
    [self SetCount:0];
    [_mSyncManager startSyncAilableDeviceAndSync:true AutoRepeat:false];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

- (void) Refresh {
    [self SetCount:0];
}



-(void) UploadJsonDataToServer:(uint32_t)ID BasicInfo:(Device_Data_Struct_t)BasicInfo rssi:(int)l_rssi record_count:(uint16_t)record_count record_data:(NSArray*)HistoryData isLastPackage:(BOOL)isLastPackage
{
    long Device_ID = ID & 0xFFFFFFFF;
    
    NSLog(@"uploading device id........%ld", Device_ID);
    
    //    NSString *historyCount = [NSString stringWithFormat:@"%hu",device.m_advertise_data.record_count];
    NSString *deviceID = [NSString stringWithFormat:@"%ld",Device_ID];
    NSString *isConnectable = @"YES";
    NSString *rssi = [NSString stringWithFormat:@"%d",l_rssi];
    NSString *batteryVoltage = [NSString stringWithFormat:@"%d", BasicInfo.battery_votage];
    NSString *signalLimit = [NSString stringWithFormat:@"%d", BasicInfo.Limit_Rssi];
    NSString *alertDelayTime = [NSString stringWithFormat:@"%d", BasicInfo.Scan_Interval];
    NSString *txPower = [NSString stringWithFormat:@"%d", BasicInfo.m_txPower];
    NSString *ShowString = @"";
    if (BasicInfo.m_alarm_type == 0 || (BasicInfo.m_alarm_type & 0x07) == 00) {
        ShowString = @"None";
    }
    else{
        if (BasicInfo.m_alarm_type & 0x01) {
            ShowString = [ShowString stringByAppendingString:@"+LED"];
        }
        if (BasicInfo.m_alarm_type & 0x02) {
            ShowString = [ShowString stringByAppendingString:@"+Buzzer"];
        }
        if (BasicInfo.m_alarm_type & 0x04) {
            ShowString = [ShowString stringByAppendingString:@"+Vibrate"];
        }
        ShowString = [ShowString substringFromIndex:1];
    }
    NSString *keepConnectMaxTime;
    NSString *latestRecordTime;
    
//    [self Update_ST_Device_State:@"Uploading..." deviceID:ID upload_count:record_count];
    [self Update_ST_Device_State:@"Uploading..." deviceID:ID upload_count:record_count UploadState:FALSE];
    
    latestRecordTime = [self dateToStringTime:[NSDate dateWithTimeIntervalSince1970:BasicInfo.record_latest_time_stamp]];
    
    
    
    if (BasicInfo.m_Connect_State == 0) {
        keepConnectMaxTime = @"No Limit";
    }
    else{
        keepConnectMaxTime = [NSString stringWithFormat:@"%d",BasicInfo.m_Connect_State];
    }
    NSString *recordCount = [NSString stringWithFormat:@"%d", record_count];
    NSString *whiteListCount = [NSString stringWithFormat:@"%d", BasicInfo.m_white_list_count];
    NSString *deviceName = [NSString stringWithFormat:@"%@", [NSString stringWithUTF8String:(char*)BasicInfo.mDevice_Name]];
    
    
    
    NSDictionary *dict;
    
    dict = @{ @"device_id" : deviceID, @"is_connectable" : isConnectable, @"rssi" : rssi, @"battery_voltage" : batteryVoltage, @"keep_connect_max_time" : keepConnectMaxTime, @"signal_limit" : signalLimit, @"alert_delay_time" : alertDelayTime, @"tx_power" : txPower, @"alerm_type" : ShowString, @"white_list_count" : whiteListCount, @"record_count" : recordCount, @"latest_record_time" : latestRecordTime, @"device_name" : deviceName, @"connection_history" : HistoryData };
    
    NSMutableArray *Temp_Array = [[NSMutableArray alloc] initWithObjects:dict, nil];
    [self uploadBeaconInfo:Temp_Array device:ID count:record_count isLastPackage:isLastPackage];
    
}


-(void)uploadBeaconInfo: (NSMutableArray*)jsonData device:(uint32_t)device_ID count:(int)count isLastPackage:(BOOL)isLastPackage{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable) {
        //connection unavailable
        [self showNoInternetMessage];
    } else {
        
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setValue:jsonData forKey:@"arrval"];
        [params setValue:@"1" forKey:@"activeCount"];//Only Upload One Device
        
        NSLog(@"param %@", params);
        //Notify Device if it is end package of records.
        if (isLastPackage) {
            [self.mSyncManager notify_record_data_upload_to_server_state_with_device_id:device_ID state:true clean_local_data:true];
        }
        
        [self Show_View_Label];
        [self Update_ST_Device_State:@"Upload Success" deviceID:device_ID upload_count:0 UploadState:TRUE];
        
        
        //        NSDictionary *dict = [jsonData valueForKey:@"arrval"];
        //        NSString *str = [NSString stringWithFormat:@"Uploading informations to server of beacon: %@", [dict valueForKey:@"device_id"]];
        //        [self->infoArray addObject:str];
        //        [self.table_view reloadData];
        
        NSError *error;
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:@"https://beaiware.net:1931/api/contactTracing"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (data != nil) {
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                            NSLog(@"json...%@",json);
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //                [self.loader stopAnimating];
                                NSString *responseCode = [NSString stringWithFormat:@"%@",json[@"response_code"]];
                                //                NSString *msg = json[@"response_message"];
                                if ([responseCode isEqualToString:@"2000"]) {
                                    
                                    //                    [self.mSyncManager notify_record_data_upload_to_server_state_with_device_id:device_ID state:true clean_local_data:true];
                                    
                //                    self.view.backgroundColor = self->greenColor;
                                    [self Show_View_Label];
                //                    [self Update_ST_Device_State:@"Upload Success" deviceID:device_ID upload_count:count];
                                    [self Update_ST_Device_State:@"Upload Success" deviceID:device_ID upload_count:count UploadState:TRUE];
                                    
                                    self->uploadCount += 1;
                                    
                                    if (self->uploadCount == self->_mUploadList.count) {
                                        self.view.backgroundColor = self->greenColor;
                                    }
                                    
                //                    NSLog(@"status...%@", [self.mUploadList[0] uploadFinished]);
                                    
                //                    for (ST_DEVICE *object in self->_mUploadList) {
                //                        if (object.uploadFinished == TRUE) {
                //                            self->uploadCount = self->uploadCount + 1;
                //                            if (self->uploadCount == self->_mUploadList.count) {
                //                                self.view.backgroundColor = self->greenColor;
                //                                break;;
                //                            }
                //                        }
                //                    }
                                    
                                    
                                    
                                    
                                }else if ([responseCode isEqualToString:@"3000"]) {
                                    
                //                    [self Update_ST_Device_State:@"Upload Fail" deviceID:device_ID upload_count:0];
                                    [self Update_ST_Device_State:@"Upload Fail" deviceID:device_ID upload_count:0 UploadState:FALSE];
                                    //                    [self createResponseJSON];//Maybe handle error, not do this here.
                                }
                            });
            }else{
                
            }
            
        }];
        
        [postDataTask resume];
    }
}

- (void) Show_View_Label
{
    NSMutableString *SHOW = [[NSMutableString alloc] init];
    self.labelBeaconCount.text = [NSString stringWithFormat:@"Tags detected: %lu", (unsigned long)_mUploadList.count];
//    [SHOW appendFormat:@"Tags detected: %lu\n", (unsigned long)_mUploadList.count];
    for (ST_DEVICE *temp in _mUploadList) {
        [SHOW appendFormat:@"\nID:%d State:%@, Upload Count:%d", temp.device_id, temp.state, temp.upload_count];
    }
    NSLog(@"Current Count:%lu", (unsigned long)_mUploadList.count);
    
//    self.labelBeaconStatus.text = SHOW;
    
    [self.m_text_show setText:SHOW];
    
}

-(void)showNoInternetMessage{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"No Internet!"
                                  message:@"Please check your internet connection!"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)errorAlert:(NSString*)errorMessage {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:errorMessage
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSString *)dateToStringTime:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (IBAction)OnTouchTotalNum:(id)sender {
    [self Refresh];
}

- (IBAction)OnTouchAbout:(id)sender {
    
    [self performSegueWithIdentifier:@"Settings" sender:nil];
}

#pragma mark - MBSYNCManagerDelegate
- (void) MBSYNCManager:(MBSYNCManager *)manager didFailToUSEBLE:(NSInteger)state Message:(NSString*)Message
{
    NSLog(@"Failed to use ble code:%ld msg:%@", (long)state, Message);
}
- (void) MBSYNCManager:(MBSYNCManager *)manager didDiscoverSTDevice:(uint32_t)Device_ID battery_voltage:(uint16_t)batt latest_record_time_stamp:(uint32_t)latest_record_time_stamp record_count:(uint16_t)record_count rssi:(int)rssi
{
    if (record_count > 0) {
//        [self Update_ST_Device_State:@"Need Upload" deviceID:Device_ID upload_count:0];
        [self Update_ST_Device_State:@"Need Upload" deviceID:Device_ID upload_count:0 UploadState:FALSE];
    }
    
    NSLog(@"Find Divice ID:%d Batt:%dmv record_count:%d", Device_ID, batt, record_count);
}

- (void) MBSYNCManager:(MBSYNCManager *)manager didFailToSyncSTDevice:(uint32_t)Device_ID error_code:(MB_SYNC_ERROR_CODE)error_code
{
    switch (error_code) {
        case MB_SYNC_ERROR_CODE_LINK_LOSS:
//            [self Update_ST_Device_State:@"Link Loss" deviceID:Device_ID upload_count:0];
            [self Update_ST_Device_State:@"Link Loss" deviceID:Device_ID upload_count:0 UploadState:FALSE];
            break;
        case MB_SYNC_ERROR_CODE_CONNECT_FAILED:
//            [self Update_ST_Device_State:@"Connect Failed" deviceID:Device_ID upload_count:0];
            [self Update_ST_Device_State:@"Connect Failed" deviceID:Device_ID upload_count:0 UploadState:FALSE];
            break;
        case MB_SYNC_ERROR_CODE_CONNECTING_TIMEOUT:
//            [self Update_ST_Device_State:@"Connect Timeout" deviceID:Device_ID upload_count:0];
            [self Update_ST_Device_State:@"Connect Timeout" deviceID:Device_ID upload_count:0 UploadState:FALSE];
            break;
        case MB_SYNC_ERROR_CODE_TRANSMISSION_ERROR:
//            [self Update_ST_Device_State:@"Transmission failed" deviceID:Device_ID upload_count:0];
            [self Update_ST_Device_State:@"Transmission failed" deviceID:Device_ID upload_count:0 UploadState:FALSE];
            break;
        case MB_SYNC_ERROR_CODE_AUTHENTICATION_FAILED:
//            [self Update_ST_Device_State:@"Auth failed" deviceID:Device_ID upload_count:0,];
            [self Update_ST_Device_State:@"Auth failed" deviceID:Device_ID upload_count:0 UploadState:FALSE];
            break;
        default:
            break;
            
            
    }
    NSLog(@"didFailToSyncSTDevice device id:%d code:%ld", Device_ID, (long)error_code);
    switch (error_code) {
        case MB_SYNC_ERROR_CODE_LINK_LOSS:
            //            NSLog(@"link loss device id:%d", Device_ID);
            break;
        case MB_SYNC_ERROR_CODE_CONNECT_FAILED:
            //            NSLog(@"connect failed device id:%d", Device_ID);
            break;
            //.....
        default:
            break;
    }
}

- (void) MBSYNCManager:(MBSYNCManager *)manager didSyncHistoryDataWithDevice:(uint32_t)Device_ID BasicInfo:(Device_Data_Struct_t)BasicInfo rssi:(int)rssi record_count:(uint16_t)record_count record_data:(NSArray*)record_data;
{
    NSLog(@"Synced %d records", record_count);
//    [self Update_ST_Device_State:@"Sync Complete" deviceID:Device_ID upload_count:0];
    [self Update_ST_Device_State:@"Sync Complete" deviceID:Device_ID upload_count:0 UploadState:FALSE];
    
    NSMutableArray *multiplePostData = [[NSMutableArray alloc] initWithArray:record_data];
  
//    //for TEST Large Data
//#ifdef TEST_LARGE_DATA
//    while (multiplePostData.count < 50000) {
//        uint32_t rand_id = rand() % 0xFFFFFF;
//        NSDictionary *DIC = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithLongLong:rand_id], @"data", @"2020-11-03 16:13:24", @"time", nil];
//        [multiplePostData addObject:DIC];
//    }
//    NSLog(@"Total Count:%lu", (unsigned long)multiplePostData.count);
//#endif
    
    if (multiplePostData.count > 5000) {
     
        int Upload_Count = (int) (multiplePostData.count / 5000);
        int LastUpload_count = multiplePostData.count % 5000;
        
        for (int i = 0; i < Upload_Count; i++) {
            if (LastUpload_count == 0 && i == (Upload_Count-1)) {
                [self UploadJsonDataToServer:Device_ID BasicInfo:BasicInfo rssi:rssi record_count:5000 record_data:[multiplePostData subarrayWithRange:NSMakeRange(i*5000, 5000)] isLastPackage:true];
            }
            else{
                [self UploadJsonDataToServer:Device_ID BasicInfo:BasicInfo rssi:rssi record_count:5000 record_data:[multiplePostData subarrayWithRange:NSMakeRange(i*5000, 5000)] isLastPackage:false];
            }            
        }
        
        //Upload Left Data
        if (LastUpload_count != 0) {
            [self UploadJsonDataToServer:Device_ID BasicInfo:BasicInfo rssi:rssi record_count:LastUpload_count record_data:[multiplePostData subarrayWithRange:NSMakeRange(Upload_Count*5000, LastUpload_count)] isLastPackage:true];
        }
    }
    
    else{
        [self UploadJsonDataToServer:Device_ID BasicInfo:BasicInfo rssi:rssi record_count:record_count record_data:record_data isLastPackage:true];
    }
}

- (void) MBSYNCManager:(MBSYNCManager *)manager didCleanedDeviceRecordDataWithState:(BOOL)State Device_ID:(uint32_t)Device_ID
{
    
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [infoArray objectAtIndex:indexPath.row];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  infoArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

//-(void) Update_ST_Device_State:(NSString*)State deviceID:(uint32_t)deviceID upload_count:(int)upload_count {
//    ST_DEVICE *temp = [[ST_DEVICE alloc] init];
//    temp.device_id = deviceID;
//    temp.state = State;
//    temp.upload_count = upload_count;
//    temp.uploadFinished = FALSE;
//
//    if (![_mUploadList containsObject:temp]) {
//        [_mUploadList addObject:temp];
//    }
//    else{
//        for (ST_DEVICE *object in _mUploadList) {
//            if ([object isEqual:temp]) {
//                object.device_id = deviceID;
//                object.state = State;
//                object.upload_count += upload_count;
//                break;
//            }
//        }
//    }
//
//    [self Show_View_Label];
//
//}

-(void) Update_ST_Device_State:(NSString*)State deviceID:(uint32_t)deviceID upload_count:(int)upload_count UploadState:(BOOL)UploadState {
    ST_DEVICE *temp = [[ST_DEVICE alloc] init];
    temp.device_id = deviceID;
    temp.state = State;
    temp.upload_count = upload_count;
    temp.uploadFinished = UploadState;
    
    if (![_mUploadList containsObject:temp]) {
        [_mUploadList addObject:temp];
    }
    else{
        for (ST_DEVICE *object in _mUploadList) {
            if ([object isEqual:temp]) {
                object.device_id = deviceID;
                object.state = State;
                object.upload_count += upload_count;
                NSLog(@"upload count...%d",upload_count);
                if (UploadState) {
                    object.uploadFinished = true;
                    
                }
                break;
            }
        }
        
    }
    
    [self Show_View_Label];
}


@end
