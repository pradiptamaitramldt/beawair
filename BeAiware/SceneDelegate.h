//
//  SceneDelegate.h
//  BeAiware
//
//  Created by Pradipta Maitra on 28/07/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

