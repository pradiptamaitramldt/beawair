//
//  ConnectionHistory.h
//  BeAiware
//
//  Created by Pradipta Maitra on 30/07/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConnectionHistory : NSObject

@property (strong, nonatomic) NSString *data;
@property (strong, nonatomic) NSString *time;

- (instancetype)initWithData:(NSString *)data andTime:(NSString *)time;

@end

NS_ASSUME_NONNULL_END
