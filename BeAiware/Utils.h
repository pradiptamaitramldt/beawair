//
//  Created by alvin on 2019/7/3.
//  Copyright © 2019 meeblue. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject


#define CSV_FENGE_CODE          @","

+ (NSString *)convertDataToHexStr:(NSData *)data;

+ (NSString *)convertCharToHexStr:(char *)data lenth:(int) Lenth;

+ (NSData*) hexStringToBytes:(NSString*)Str;

+ (NSString *)DateToStringTime:(NSDate *)date;

+ (uint32_t) RecentSensorTime:(NSMutableArray *)Data;

+ (NSString *)SaveToStringTime:(NSDate *)date;

+ (NSString *)CreatRecordExportTempFile:(NSData *)Data;

+ (NSString *)dateToString:(NSDate *)date;

+ (void)shareData:(NSURL *)fileURL View:(UIViewController*)View;

@end





NS_ASSUME_NONNULL_END
