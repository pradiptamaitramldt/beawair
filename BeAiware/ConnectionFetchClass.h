//
//  ConnectionFetchClass.h
//  BeAiware
//
//  Created by Carlos Monero on 10/6/20.
//  Copyright © 2020 beaiware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Reachability.h"
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConnectionFetchClass : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelBeaconStatus;

@end

NS_ASSUME_NONNULL_END
