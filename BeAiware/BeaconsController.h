//
//  Created by alvin on 19-10-22.
//  Copyright (c) 2019 meeblue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//#import "AFNetworking.h"
//#import "SVProgressHUD.h"
#import "Reachability.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <ST_SYNC_SDK/MBSYNCManager.h>

@interface BeaconsController : UIViewController<UIScrollViewDelegate, MBSYNCManagerDelegate, NSURLSessionDelegate>
{
    AppDelegate *appDelegate;
    int beaconCount;
    int deviceFOundcont;
    int uploadCount;
    UIColor *yellowColor;
    UIColor *orangeColor;
    UIColor *greenColor;
    NSMutableArray *infoArray;
}

@property (weak, nonatomic) IBOutlet UILabel *labelBeaconStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelBeaconCount;
@property (weak, nonatomic) IBOutlet UITableView *table_view;


//@property (weak, nonatomic) IBOutlet UIButton *mBeaconCountView;

//- (IBAction)OnTouchTotalNum:(id)sender;

//- (IBAction)OnTouchAbout:(id)sender;

//@property (weak, nonatomic) IBOutlet UITableView *mDataTableView;
@end
