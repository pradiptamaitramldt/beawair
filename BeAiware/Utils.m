//
//  Created by alvin on 19-10-22.
//  Copyright (c) 2019 meeblue. All rights reserved.
//

#import "Utils.h"
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

typedef struct
{
    uint32_t time;
    uint32_t data;
} Sensor_data_t;

@interface Utils ()

@end

@implementation Utils

+ (NSData*) hexStringToBytes:(NSString*)Str {
    
    NSMutableData *append = [[NSMutableData alloc]init];
    int idx;
    for (idx = 0; idx+2 <= Str.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [Str substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [append appendBytes:&intValue length:1];
    }
    return append;
}

+ (NSString *)SaveToStringTime:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

+ (NSString *)CreatRecordExportTempFile:(NSData *)Data
{
    NSString *MAC = [Utils convertDataToHexStr:Data].uppercaseString;
    
    NSString *fileNameStr = [NSString stringWithFormat:@"%@_History_%@.csv", MAC, [Utils SaveToStringTime:[NSDate date]]];
    NSString *DocPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:fileNameStr];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:DocPath]){
        [ fileManager removeItemAtPath:DocPath error:NULL];
    }
    
    return DocPath;
}

+ (NSString *)convertDataToHexStr:(NSData *)data {

    if (!data || [data length] == 0) {

        return @"";
    }

     __block NSString *string = @"";

 
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {

        unsigned char *dataBytes = (unsigned char*)bytes;

        for (NSInteger i = 0; i < byteRange.length; i++) {

            NSString*hexStr= [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];

            if([hexStr length] == 2){

                string = [string stringByAppendingString:hexStr];

            } else{

                string = [string stringByAppendingString:[NSString stringWithFormat:@"0%@", hexStr]];

            }

        }

    }];
    return string;
}

+ (NSString *)convertCharToHexStr:(char *)data lenth:(int) Lenth {

    if (!data || Lenth == 0) {

        return @"";
    }

     __block NSString *string = @"";
    
    unsigned char *dataBytes = (unsigned char*)data;

    for (NSInteger i = 0; i < Lenth; i++) {

        NSString*hexStr= [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];

        if([hexStr length] == 2){

            string = [string stringByAppendingString:hexStr];

        } else{

            string = [string stringByAppendingString:[NSString stringWithFormat:@"0%@", hexStr]];

        }

    }
 
    return string;
}

+ (NSString *)DateToStringTime:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}



+ (uint32_t) RecentSensorTime:(NSMutableArray *)Data
{
    if (Data == nil || Data.count == 0) {
        return 0;
    }
    
    Sensor_data_t Value;
    uint64_t TEMP = [[Data firstObject] unsignedLongLongValue];
    memcpy(&Value, &TEMP, sizeof(Sensor_data_t));
    if (Value.time > 60) {
        Value.time -= 60;
    }
    return Value.time;
}

+ (NSString *)dateToString:(NSDate *)date {
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss.SSS"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}


+ (void)shareData:(NSURL *)fileURL View:(UIViewController*)View{

    NSArray *urls=@[fileURL];

    UIActivityViewController *activituVC=[[UIActivityViewController alloc]initWithActivityItems:urls applicationActivities:nil];

    
    // iphone
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [View presentViewController:activituVC animated:YES completion:nil];
    }
    else // ipad Need PopOver
    {
        // support iPad
        activituVC.popoverPresentationController.sourceView = View.view;
        activituVC.popoverPresentationController.sourceRect = View.view.frame;
        
        //show
        [View presentViewController:activituVC animated:YES completion:nil];
    }

}
@end
